#provider "azurerm" {
#  features {}
  #subscription_id = "0a1fca4b-fe80-493c-945b-bf30cd34a94f"
#}

provider "azurerm" {
  features {}
  subscription_id = "${var.sub}"
}

terraform {
  backend "azurerm" {
    resource_group_name = "tfstate"
    storage_account_name = "tfstatedavid"
    container_name = "tfstate"
    key = "cdavid.terraform.tfstate"
  }
}
