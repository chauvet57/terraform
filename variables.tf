variable "prefix" {
  # A personnaliser avec votre nom
  default = "david"
}

variable "environment" {
  default = "demo"
}

variable "project" {
  default = "MEWO"
}

variable "sshkey" {
  default = ""
}

variable "sub" {
  default = ""
}
